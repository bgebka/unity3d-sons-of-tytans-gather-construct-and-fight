# Welcome!

I am pleased to welcome you in my scripts repository.
Above you will find a collection of scripts from my new, under development Unity3D project named: Gather, Constuct and Fight!

So far, I've managed to implement a few new mechanics:

- Gathering Resources
- State machine with State Factory
- Movement
- Resource resistance rebuilding after pasue in gathering process

# UNITY3d Sons of Tytans - Gather Construct and Fight

My first big unity game 3d project. Sons of Tytans. In concept will be a multiplayer, competitive game with third person controller. 
Gameplay is divided into 3 different mechanical stages : Gathering Resources, Base buidling and ofc fighting ;)

