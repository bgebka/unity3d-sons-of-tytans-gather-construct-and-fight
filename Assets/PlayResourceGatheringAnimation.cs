using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayResourceGatheringAnimation : MonoBehaviour
{
    [SerializeField] private SO_MeleeRangeInfoBase _meleeRangeList;
    private Animator _resourceAnimator;
    private GameObject _onPointingAt;
    private GameObject Player;

    public void Start()
    {
        _resourceAnimator = GetComponent<Animator>();
        Player = GameObject.Find(nameof(Player));
    }
    private void OnEnable()
    {
        EventsCollection.OnResourceHit.AddListener(PlayAnimation);
    }

    private void OnDisable()
    {
        EventsCollection.OnResourceHit.RemoveListener(PlayAnimation);
    }

    private void PlayAnimation(GameObject strucedResource)
    {
        _onPointingAt = Player.GetComponent<PlayerAimController>().GetObjectWhichAimingFor();

        if(_onPointingAt == this.gameObject && _meleeRangeList.ContainsGameObject(this.gameObject))
        {
            _resourceAnimator.Play("ResourceGatheringHitShaking");
        }
    }
}
