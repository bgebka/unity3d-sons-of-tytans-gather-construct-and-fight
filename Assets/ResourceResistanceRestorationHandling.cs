using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceResistanceRestorationHandling : MonoBehaviour
{
    private bool _isCollected = false;

    private SO_Resource _identifierResoureType;
    private ResourceIdentifier _identifier;

    private Coroutine _waitUntilDelayEnd;
    private Coroutine _resourceRegeneration;

    [SerializeField] private SO_ResourceManager ResourceManager;

    [SerializeField] private float _delayToStartRegeneration = 3.0f;
    [SerializeField] private float _delayBetweenRegenerationCycle = 1f;

    [Range(0.0f, 1.0f)]
    [SerializeField] private float _HealthPercentageRagenerationPerCycle = 20.0f;

    private void OnEnable()
    {
        EventsCollection.AfterResourceHit.AddListener(CollectionPending);
    }

    private void OnDisable()
    {
        EventsCollection.AfterResourceHit.RemoveListener(CollectionPending);
    }

    private void Start()
    {
        if(TryGetComponent<ResourceIdentifier>(out ResourceIdentifier identifier))
        {
            _identifier = identifier;
        }
        var resourceName = _identifier.ResourceName;
        if (ResourceManager.ResourcesTypes != null)
        {
            if (ResourceManager.ResourcesTypes.ContainsKey(resourceName))
                _identifierResoureType = ResourceManager.ResourcesTypes[resourceName];
        }
    }

    private void CollectionPending(GameObject gameObject)
    {
        _isCollected = true;

        if(_waitUntilDelayEnd != null)  StopCoroutine(_waitUntilDelayEnd);
        if(_resourceRegeneration != null) StopCoroutine(_resourceRegeneration);

         _waitUntilDelayEnd =  StartCoroutine(WaitUntilDelayEnd(_delayToStartRegeneration));
    }

    private IEnumerator WaitUntilDelayEnd(float delay)
    {

        yield return new WaitForSecondsRealtime(delay);

        _isCollected = false;

        if(_identifier.Resistance < _identifierResoureType.Resistance)
        _resourceRegeneration = StartCoroutine(ResourceRegeneration(_delayBetweenRegenerationCycle));

    }

    private IEnumerator ResourceRegeneration(float regenerationDelay)
    {
        var regenerationValue = (_HealthPercentageRagenerationPerCycle / 100.0f) * _identifierResoureType.Resistance;
        var healthDelta = _identifierResoureType.Resistance - _identifier.Resistance;

        if (healthDelta <= regenerationValue) _identifier.Resistance = _identifierResoureType.Resistance;
        else
        {
            _identifier.Resistance += regenerationValue;
        }

        yield return new WaitForSecondsRealtime(regenerationDelay);

        if(_identifier.Resistance != _identifierResoureType.Resistance)
            _resourceRegeneration = StartCoroutine(ResourceRegeneration(_delayBetweenRegenerationCycle));
    }
}
