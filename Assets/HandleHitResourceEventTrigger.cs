using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleHitResourceEventTrigger : MonoBehaviour
{
    public void HitResourceTrigger()
    {
        var playerObjectInfo = this.transform.root.gameObject;
        EventsCollection.OnResourceHit?.Invoke(playerObjectInfo);
    }
}
