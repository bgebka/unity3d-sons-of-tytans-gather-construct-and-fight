using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerActionStateMachine))]
public class PlayerController : MonoBehaviour
{
    // variable to store inputSystem
    private InputController _input;

    [SerializeField]
    Vector3 test;

    
    // variable to store character animation component
    private Animator _animator;
    bool _hasAnimator;

    // state machine/manager
    private PlayerActionStateMachine _stateMachine;

    private Quaternion _positionBeforeGathering = Quaternion.identity;

    // timeout deltatime
    private float _jumpTimeoutDelta;
    private float _fallTimeoutDelta;

    // variable to store optimized setter/getter parameter animation IDs
    int isWalkingHash;
    int isRunningHash;
    int _animIDSpeed;
    int _animIDMotionSpeed;
    int _animIDGrounded;
    int _animIDJump;
    int _animIDFreeFall;

    // viarables to store actual data from InputSystem

    private Vector2 _playerMovementDirection;
    private Vector2 _cameraRotation;
    private float _threshold = 0.01f;
    private bool _isSprinting;
    private bool _isJumping;
    private float _animationBlend;



    [SerializeField]
    private GameObject aimPointTrigger;


    [Tooltip("Acceleration and deceleration")]
    public float SpeedChangeRate = 10.0f;

    private CharacterController _controller;
    public GameObject CinemachineCameraTarget;
    public Camera _mainCamera;

    private float _cinemachineTargetYaw;
    private float _cinemachineTargetPitch;
    private float BottomClamp = -50.0f;
    private float TopClamp = 85.0f;
    private float CameraAngleOverride = 0.0f;
    private float _targetRotation = 0.0f;
    private float _speed;
    private float _rotationVelocity;
    private float _verticalVelocity;
    private float _terminalVelocity = 53.0f;

    [SerializeField]
    private bool Grounded;

    [Header("Player")]
    [Tooltip("Move speed in m/s")]
    public float moveSpeed = 2.0f;

    [Header("Player")]
    [Tooltip("Move speed in m/s")]
    public Animator meleeAnimator;

    [Tooltip("Sprint speed of the character in m/s")]
    public float sprintSpeed = 5.35f;

    [Tooltip("How fast the character turns to face movement direction")]
    [Range(0.0f, 0.3f)]
    public float RotationSmoothTime = 0.12f;

    [Space(10)]
    [Tooltip("The height the player can jump")]
    public float JumpHeight = 1.2f;

    [Space(10)]
    [Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
    public float JumpTimeout = 0.50f;

    [Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
    public float FallTimeout = 0.15f;

    [Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
    public float GroundedRadius = 0.28f;

    [Tooltip("Useful for rough ground")]
    public float GroundedOffset = -0.14f;

    [Tooltip("What layers the character uses as ground")]
    public LayerMask GroundLayers;

    [Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
    public float Gravity = -15.0f;

    private void Update()
    {
        JumpAndGravity();
        GroundedCheck();
        Move();
    }

    private void Start()
    {
        if(TryGetComponent<PlayerActionStateMachine>(out var playerStateManager))
        {
            _stateMachine = playerStateManager;
        }
        _controller = GetComponent<CharacterController>();
        _cinemachineTargetYaw = CinemachineCameraTarget.transform.eulerAngles.y;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void LateUpdate()
    {
        CameraRotation();
    }
    private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }



    private void GroundedCheck()
    {
        // set sphere position, with offset
        Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset,
            transform.position.z);
        Grounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers,
            QueryTriggerInteraction.Ignore);

        // update animator if using character
        if (_hasAnimator)
        {
            _animator.SetBool(_animIDGrounded, Grounded);
        }
    }
    private void Move()
    {
        float targetSpeed = ChooseTargetSpeed();
        targetSpeed = ResetSpeedIfDontGetInput(targetSpeed);
        float currentHorizontalSpeed = GetCurrentHorizontalSpeed();

        float speedOffset = 0.1f;
        float inputMagnitude = _playerMovementDirection.magnitude;

        if (CheckSpeedsIsGreaterThenOffset(targetSpeed, currentHorizontalSpeed, speedOffset))
        {
            // creates curved result rather than a linear one giving a more organic speed change
            // note T in Lerp is clamped, so we don't need to clamp our speed
            LerpPlayerSpeed(targetSpeed, currentHorizontalSpeed, inputMagnitude);
        }
        else
        {
            _speed = targetSpeed;
        }

        // normalise input direction
        Vector3 inputDirection = new Vector3(_playerMovementDirection.x, 0.0f, _playerMovementDirection.y).normalized;

        // note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
        // if there is a move input rotate player when the player is moving
        if (_playerMovementDirection != Vector2.zero)
        {
            _targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg +
                          _mainCamera.transform.eulerAngles.y;
            float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetRotation, ref _rotationVelocity,
                RotationSmoothTime);

            //rotate to face input direction relative to camera position
            transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
        }

        if ( aimPointTrigger.activeSelf && _stateMachine.CurrentState == _stateMachine.GetState<ActionGatheringState>())
        {
            var direction = (aimPointTrigger.transform.position - transform.position).normalized;
            Quaternion rotgoal = Quaternion.LookRotation(direction);

            rotgoal = Quaternion.Euler(0, rotgoal.eulerAngles.y, 0);

            transform.rotation = Quaternion.Slerp(transform.rotation, rotgoal, 0.01f);
        }

        Vector3 targetDirection = Quaternion.Euler(0.0f, _targetRotation, 0.0f) * Vector3.forward;

        // move the player
        _controller.Move(targetDirection.normalized * (_speed * Time.deltaTime) +
                         new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);

        // update animator if using character
        if (_hasAnimator)
        {
            _animator.SetFloat(_animIDSpeed, _animationBlend);
            _animator.SetFloat(_animIDMotionSpeed, inputMagnitude);
        }
    }

    private void LerpPlayerSpeed(float targetSpeed, float currentHorizontalSpeed, float inputMagnitude)
    {
        _speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude,
                        Time.deltaTime * SpeedChangeRate);

        // round speed to 3 decimal places
        _speed = Mathf.Round(_speed * 1000f) / 1000f;
    }

    private float ResetSpeedIfDontGetInput(float targetSpeed)
    {
        if (_playerMovementDirection == Vector2.zero) targetSpeed = 0.0f;
        return targetSpeed;
    }

    private float GetCurrentHorizontalSpeed()
    {

        // a reference to the players current horizontal velocity
        return new Vector3(_controller.velocity.x, 0.0f, _controller.velocity.z).magnitude;
    }

    private float ChooseTargetSpeed()
    {
        // set target speed based on move speed, sprint speed and if sprint is pressed
        return _isSprinting ? sprintSpeed : moveSpeed;
    }

    private static bool CheckSpeedsIsGreaterThenOffset(float targetSpeed, float currentHorizontalSpeed, float speedOffset)
    {
        return currentHorizontalSpeed < targetSpeed - speedOffset ||
                    currentHorizontalSpeed > targetSpeed + speedOffset;
    }

    private void OnDrawGizmosSelected()
    {
        Color transparentGreen = new Color(0.0f, 1.0f, 0.0f, 0.35f);
        Color transparentRed = new Color(1.0f, 0.0f, 0.0f, 0.35f);

        if (Grounded) Gizmos.color = transparentGreen;
        else Gizmos.color = transparentRed;

        // when selected, draw a gizmo in the position of, and matching radius of, the grounded collider
        Gizmos.DrawSphere(
            new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z),
            GroundedRadius);
    }
    private void JumpAndGravity()
    {
        if (Grounded)
        {
            // reset the fall timeout timer
            _fallTimeoutDelta = FallTimeout;

            // update animator if using character
            if (_hasAnimator)
            {
                _animator.SetBool(_animIDJump, false);
                _animator.SetBool(_animIDFreeFall, false);
            }

            // stop our velocity dropping infinitely when grounded
            if (_verticalVelocity < 0.0f)
            {
                _verticalVelocity = -2f;
            }

            // Jump
            if (_isJumping && _jumpTimeoutDelta <= 0.0f)
            {
                // the square root of H * -2 * G = how much velocity needed to reach desired height
                _verticalVelocity = Mathf.Sqrt(JumpHeight * -2f * Gravity);

                // update animator if using character
                if (_hasAnimator)
                {
                    _animator.SetBool(_animIDJump, true);
                }
            }

            // jump timeout
            if (_jumpTimeoutDelta >= 0.0f)
            {
                _jumpTimeoutDelta -= Time.deltaTime;
            }
        }
        else
        {
            // reset the jump timeout timer
            _jumpTimeoutDelta = JumpTimeout;

            // fall timeout
            if (_fallTimeoutDelta >= 0.0f)
            {
                _fallTimeoutDelta -= Time.deltaTime;
            }
            else
            {
                // update animator if using character
                if (_hasAnimator)
                {
                    _animator.SetBool(_animIDFreeFall, true);
                }
            }

            // if we are not grounded, do not jump
            _isJumping = false;
        }

        // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
        if (_verticalVelocity < _terminalVelocity)
        {
            _verticalVelocity += Gravity * Time.deltaTime;
        }
    }
    private void CameraRotation()
    {
        // if there is an input and camera position is not fixed
        if (_cameraRotation.sqrMagnitude >= _threshold )
        {
            var _inputValue = _cameraRotation;

            //Don't multiply mouse input by Time.deltaTime;

            _cinemachineTargetYaw += _inputValue.x;
            _cinemachineTargetPitch += _inputValue.y;
        }

        // clamp our rotations so our values are limited 360 degrees
        _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
        _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

        // Cinemachine will follow this target
        CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + CameraAngleOverride,
            _cinemachineTargetYaw, 0.0f);
    }
    private void Awake()
    {
        _input = new InputController();

        _input.Player.Move.performed += ctx => _playerMovementDirection = ctx.ReadValue<Vector2>();
        _input.Player.Move.canceled += (ctx) => _playerMovementDirection = Vector2.zero;

        _input.Player.Look.performed += ctx => _cameraRotation = ctx.ReadValue<Vector2>();
        _input.Player.Sprint.performed += ctx => _isSprinting = ctx.ReadValueAsButton();
        _input.Player.Jump.started += ctx => _isJumping = ctx.ReadValueAsButton();

    }

    private void AddAnimationsIDsToHash()
    {
        _hasAnimator = TryGetComponent<Animator>(out _animator);

        if (!_hasAnimator)
            return;
        else
        {
            isWalkingHash = Animator.StringToHash("isWalking");
            isRunningHash = Animator.StringToHash("isRunning");       
        }
    }

   
    private void OnEnable()
    {
        _input.Player.Enable();
    }

    private void OnDisable()
    {
        _input.Player.Disable();
    }
}


