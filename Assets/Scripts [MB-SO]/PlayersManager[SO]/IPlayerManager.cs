using System.Collections.Generic;
using UnityEngine;
public interface IPlayerManager 
{
    public List<GameObject> FindAvailablePlayers();
}
