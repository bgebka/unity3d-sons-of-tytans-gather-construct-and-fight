using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "PlayersManager", menuName = "Managers/PlayersManager", order = 1)]
public class SO_PlayerManager : ScriptableObject, IPlayerManager
{
    [SerializeField] private List<GameObject> _availablePlayers;
    private string Player;

    public List<GameObject> FindAvailablePlayers()
    {
        InstaniateCollections();
        return  _availablePlayers = GameObject.FindGameObjectsWithTag(nameof(Player)).ToList();
    }

    private void InstaniateCollections()
    {
        if (_availablePlayers != null) return;
        else
        {
            _availablePlayers = new List<GameObject>();
        }
    }
    
    private void Reset()
    {
        FindAvailablePlayers();
    }

    private void OnEnable()
    {
        FindAvailablePlayers();
    }

    private void OnValidate()
    {
        FindAvailablePlayers();
    }

}
