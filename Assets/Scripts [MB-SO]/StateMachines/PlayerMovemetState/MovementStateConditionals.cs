using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementStateConditionals 
{
    PlayerActionStateMachine _playerStateManager;
    bool _isInitalized;

    bool _isGathering;
    GameObject _objectAimingAt;
    List<GameObject> _objectsInMeleeRange;

    public MovementStateConditionals(PlayerActionStateMachine _stateManagerReference)
    {
        Initalize(_stateManagerReference);
    }

    private void Initalize(PlayerActionStateMachine _stateManagerReference)
    {
        _playerStateManager = _stateManagerReference;
        if (!_isInitalized && _playerStateManager != null)
        {
            _isInitalized = true;
        }
    }

    private void AssignGatheringData()
    {
        _isGathering = _playerStateManager.GatheringData._isGatheringBool;
        _objectAimingAt = _playerStateManager.AimingData._beignCollectedObject;
        _objectsInMeleeRange = _playerStateManager.AimingData._objectsInMeleeRange;
    }

    public bool IdleToGatheringConditional()
    {
        AssignGatheringData();

        return !_isGathering ||
                _objectAimingAt == null ||
               !_objectsInMeleeRange.Contains(_objectAimingAt);
    }
}
