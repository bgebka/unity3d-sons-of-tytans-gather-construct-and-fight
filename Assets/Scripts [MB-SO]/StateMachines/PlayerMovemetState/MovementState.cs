public abstract class MovementState : BaseState
{
    public PlayerMovementStateMachine StateMachine { get; set; }
    public MovementState(PlayerMovementStateMachine stateMachine)
    {
        StateMachine = stateMachine;
    }
}
