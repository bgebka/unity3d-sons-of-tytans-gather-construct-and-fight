using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementIdleeState : MovementState
{
    public MovementIdleeState(PlayerMovementStateMachine stateMachine) : base(stateMachine)
    {
        base.StateMachine = stateMachine;
    }

    
    public override void ActiveTransitions()
    {
        StateMachine.SetTransition<MovementIdleeState, MovementJumpingState>(true);
        StateMachine.SetTransition<MovementIdleeState, MovementRunningState>(true);
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {

    }

    public override void Exit()
    {

    }

    public override void OnCollisionEnter()
    {

    }

    public override void UpdateState()
    {

    }

   
}
