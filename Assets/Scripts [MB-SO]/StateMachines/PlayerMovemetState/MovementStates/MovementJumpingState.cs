using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementJumpingState : MovementState
{
    public MovementJumpingState(PlayerMovementStateMachine stateMachine) : base(stateMachine)
    {
        base.StateMachine = stateMachine;
    }

    public override void ActiveTransitions()
    {
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {
    }

    public override void Exit()
    {
    }

    public override void OnCollisionEnter()
    {
    }

    public override void UpdateState()
    {
    }
}
