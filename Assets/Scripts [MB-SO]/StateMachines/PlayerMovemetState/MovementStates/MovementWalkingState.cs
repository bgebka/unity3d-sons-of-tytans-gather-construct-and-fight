using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementWalkingState : MovementState
{
    public MovementWalkingState(PlayerMovementStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void ActiveTransitions()
    {
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {
    }

    public override void Exit()
    {
    }

    public override void OnCollisionEnter()
    {
    }

    public override void UpdateState()
    {
    }
}
