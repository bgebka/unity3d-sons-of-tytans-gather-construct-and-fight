using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public abstract class BaseStateMachine: MonoBehaviour 
{
    protected BaseState _currentState;

    //Transitions and States Collections
    protected Dictionary<StateTransition, bool> _transitionsSelector;
    protected List<StateTransition> _allPosibleTransitions;
    protected static List<BaseState> _states;
    protected static Dictionary<Type, BaseState> _statesByType;

    private bool IsInitalized => _allPosibleTransitions != null && _states != null && _statesByType != null && _transitionsSelector != null;
    protected void InitalizeStateMachine<T>() where T: BaseState
    {
        if (IsInitalized) return;

        InstantiateStatesForType<T>();
        InstantiateAllPosibleTransitions();
        InstantiateTransitionsSelector();
    }

    public virtual void SwitchState(BaseState state)
    {
        var tempState = FindTransition(new StateTransition(_currentState, state));
        TransitionsSelectorUpdate();

        if (!_transitionsSelector[tempState])
        {
            Debug.LogError($"Transition from {_currentState.GetType().FullName} to {state.GetType().FullName} was not activated!");
            return;
        }
        if (state != null)
        {
            if(_currentState != null)
            {
                _currentState.Exit();
            }
            _currentState = state;
            _currentState.EnterState();
        }
    }

    public virtual void TransitionsSelectorUpdate()
    {
        if(_states == null)
        {
            Debug.LogError($"State List was not initalized before SwitchState call in child state machine");
            return;
        }

        foreach (var stateType in _states)
        {
            stateType.ActiveTransitions();
        }
    }
    public virtual StateTransition FindTransition(BaseState start, BaseState end) 
    {
        return _allPosibleTransitions.First(x => x == new StateTransition(start, end));
    }

    public virtual StateTransition FindTransition(Type start, Type end) 
    {
        return _allPosibleTransitions.First(x => x == new StateTransition(start, end));
    }
    public virtual StateTransition FindTransition(StateTransition stateTransition)
    {
        return _allPosibleTransitions.First(x => x == stateTransition);
    }

    protected virtual void InstantiateAllPosibleTransitions()
    {
        if (_allPosibleTransitions != null) return;

        _allPosibleTransitions = new List<StateTransition>();

        if(_states == null)
        {
            Debug.LogError($"{nameof(InstantiateStatesForType)} has to be called before {nameof(InstantiateAllPosibleTransitions)}");
        }
        foreach (var stateType in _states)
        {
            for (int i = 0; i < _states.Count; i++)
            {
                if (stateType != _states[i])
                {
                    _allPosibleTransitions.Add(new StateTransition(stateType, _states[i]));
                }
            }
        }
    }
    protected virtual void InstantiateTransitionsSelector()
    {
        if (_allPosibleTransitions == null) InstantiateAllPosibleTransitions();
        _transitionsSelector = new Dictionary<StateTransition, bool>();

        foreach (var transition in _allPosibleTransitions)
        {
            _transitionsSelector.Add(transition, false);
        }
    }

    protected void InstantiateStatesForType<T>() where T: BaseState
    {
        if (_states != null && _statesByType != null) return;

        _states = new List<BaseState>();
        _statesByType = new Dictionary<Type, BaseState>();

         var types = Assembly.GetAssembly(typeof(T)).GetTypes()
                                                    .Where(stateType => stateType.IsClass &&
                                                                        !stateType.IsAbstract &&
                                                                        stateType.IsSubclassOf(typeof(T))).ToList();
        foreach (var type in types)
        {
            var genericArguments = type.GetGenericArguments();
            Debug.Log($"Generis parameter: {genericArguments}");

            var instance = Activator.CreateInstance(type, new object[] { this }) as BaseState;
            _statesByType.Add(type, instance);
            _states.Add(instance);
        }
    }

    public void SetTransition(BaseState start, BaseState end, bool value) 
    {
        var newTransition = FindTransition(start, end);

        _transitionsSelector[newTransition] = value;
    }

    public void SetTransition<T, F> (bool value) where T: BaseState
    {
        var tempTransition = FindTransition(typeof(T), typeof(F));
        _transitionsSelector[tempTransition] = value;
    }

    public void SetTransition(StateTransition stateTransition, bool value)
    {
        _transitionsSelector[stateTransition] = value;
    }

    public T GetState<T>() where T: BaseState
    {
        if(_statesByType.ContainsKey(typeof(T)))
        {
            return (T)_statesByType[typeof(T)];
        }
        else
        {
            Debug.LogError($"State type: {typeof(T)} doeasn't exist in _state container");
            return null;
        }
    }

    public T GetStateInstance<T>() where T: BaseState
    {
        return null ;
    }
    
}
