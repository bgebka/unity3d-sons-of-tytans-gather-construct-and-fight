using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

[RequireComponent(typeof(PlayerAimController), typeof(GatheringResourcesController))]
public class PlayerActionStateMachine : BaseStateMachine
{
    #region variables and references

    //Variable is used as debug inspector field
    [SerializeField] public string currentStateName;

    private ActionStateConditionals _transitionConditionals;

    //DataBanks
    PlayerGatheringData _gatheringData;
    PlayerAimingData _aimingData;

    //Component and Assets references
    PlayerAimController _playerAimController;
    GatheringResourcesController _resourcePlayerGatheringController;
    [SerializeField] private SO_MeleeRangeInfoBase _meleeRangeInfoBase;


    public ActionStateConditionals TransitionsConditionals { get => _transitionConditionals; }
    public BaseState CurrentState { get => base._currentState; }

    public PlayerGatheringData GatheringData
    {
        get => _gatheringData;
        private set => _gatheringData = value;
    }
    public PlayerAimingData AimingData
    {
        get => _aimingData;
        set => _aimingData = value;
    }

    private void Start()
    {
        SetStartingOptions();
    }

    private void SetStartingOptions()
    {
        base._currentState = GetState<ActionIdleState>();
    }

    private void Update()
    {
        UpdateData();
        
        if (base._currentState != null)
        {
            AssignCurrentStateName(base._currentState);
            base._currentState.UpdateState();
        }
    }

    private void Awake()
    {
        InitalizeStateMachine<PlayerActionState>();
        
        InitalizeTransitionsConditionals();
        GetComponentsReferences();
    }
   
    private void InitalizeTransitionsConditionals()
    {
        _transitionConditionals = new ActionStateConditionals(this);
    }


    private void ActivateTransitions()
    {

    }

    private void GetComponentsReferences()
    {
        if (TryGetComponent<GatheringResourcesController>(out var gatheringResourcesController))
        {
            _resourcePlayerGatheringController = gatheringResourcesController;
        }
        else
        {
            Debug.LogError($"Root does not have {typeof(GatheringResourcesController)} component!");
        }

        if (TryGetComponent<PlayerAimController>(out var playerAimController))
        {
            _playerAimController = playerAimController;
        }
        else
        {
            Debug.LogError($"Root does not have {typeof(PlayerAimController)} component!");
        }
    }

    #endregion

    private void AssignCurrentStateName(BaseState state)
    {
        currentStateName = String.Format("CurrentState: {0}.", state);
    }

    #region ConditionalData
    public struct PlayerGatheringData
    {
        public bool _isGatheringBool;
    }
    public struct PlayerAimingData
    {
        public GameObject _beignCollectedObject;
        public List<GameObject> _objectsInMeleeRange;
    }

    private void UpdateData()
    {
        UpdateGatheringData();
        UpdateAimingData();
    }

    private void UpdateGatheringData()
    {
        _gatheringData._isGatheringBool = _resourcePlayerGatheringController.GetGatheringState;
    }

    private void UpdateAimingData()
    {
        _aimingData._beignCollectedObject = _playerAimController.GetObjectWhichAimingFor();
        _aimingData._objectsInMeleeRange = _meleeRangeInfoBase.GameObjectsInMeleeRangeList;
    }

    #endregion

}
