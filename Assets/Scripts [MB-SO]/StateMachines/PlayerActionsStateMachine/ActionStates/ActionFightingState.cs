using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionFightingState : PlayerActionState
{
    public ActionFightingState(PlayerActionStateMachine stateMachine) 
    {
        this.StateMachine = stateMachine;
    }

    public override PlayerActionStateMachine StateMachine { get; set; }

    public override void ActiveTransitions()
    {

    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {
    }

    public override void Exit()
    {
    }

    public override void OnCollisionEnter()
    {
    }

    public override void UpdateState()
    {
    }
}
