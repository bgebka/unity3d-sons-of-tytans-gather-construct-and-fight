using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionGatheringState : PlayerActionState
{
  
    public ActionGatheringState(PlayerActionStateMachine stateMachine) 
    {
        this.StateMachine = stateMachine;
    }

    public override PlayerActionStateMachine StateMachine { get; set; }

    public override void ActiveTransitions()
    {
        StateMachine.SetTransition(StateMachine.GetState<ActionGatheringState>(), StateMachine.GetState<ActionIdleState>(), true);
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {
    }
  
    public override void Exit()
    {
    }
    public override void OnCollisionEnter()
    {
    }

    public override void UpdateState()
    {
        if(StateMachine.TransitionsConditionals.IdleToGatheringConditional())
        {
            StateMachine.SwitchState(StateMachine.GetState<ActionIdleState>());
        }
    }
}
