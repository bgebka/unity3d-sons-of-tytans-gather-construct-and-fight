using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionIdleState : PlayerActionState
{
    public ActionIdleState(PlayerActionStateMachine stateMachine) 
    {
        this.StateMachine = stateMachine;
    }

    public override PlayerActionStateMachine StateMachine { get ; set ; }

    public override void ActiveTransitions()
    {
        StateMachine.SetTransition(this, StateMachine.GetState<ActionGatheringState>(), true);
    }
    public override void UpdateState()
    {
        var conditionals = StateMachine.TransitionsConditionals;

        if(!conditionals.IdleToGatheringConditional())
        {
            StateMachine.SwitchState(StateMachine.GetState<ActionGatheringState>());
        }
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {

    }

    public override void Exit()
    {
    }

    public override void OnCollisionEnter()
    {
        

    }

   
}
