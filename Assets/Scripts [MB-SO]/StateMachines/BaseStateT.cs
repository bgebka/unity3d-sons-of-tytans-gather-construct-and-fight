using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseStateT<T> : BaseState where T: new()
{
    public abstract T StateMachine { get; set; }
}
