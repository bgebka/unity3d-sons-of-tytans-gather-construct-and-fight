using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

[CustomEditor(typeof(SO_Resource))]
public class SO_ResourceEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SO_Resource myResource = (SO_Resource)target;
        if (GUILayout.Button("SaveName"))
        {
            myResource.RenameScripltableObjectAsset();
        }
    }
}

#endif
