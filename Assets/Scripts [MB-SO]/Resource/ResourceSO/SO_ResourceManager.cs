using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[CreateAssetMenu(fileName ="ResourceManager", menuName = "Managers/ResourceManager", order = 1)]
public class SO_ResourceManager: ScriptableObject, ISingletonManager
{
    private Dictionary<string, SO_Resource> _resourcesTypes;
    [SerializeField] private List<string> _resourcesNames;

    public Dictionary<string, SO_Resource> ResourcesTypes { get => _resourcesTypes; private set => _resourcesTypes = value; }
    public List<string> ResourcesNames { get => _resourcesNames; private set => _resourcesNames = value; }

    private void InitalizeCollections()
    {
        if (_resourcesTypes != null && _resourcesNames != null) return;
        else
        {
            _resourcesTypes = new Dictionary<string, SO_Resource>();
            _resourcesNames = new List<string>();
        }
    }
    void Reset()
    {
        FindAllCreatedResources();
    }
    private void OnValidate()
    {
        FindAllCreatedResources();
    }
    private void OnEnable()
    {
        FindAllCreatedResources();  
        EventsCollection.OnAssetCreate.AddListener(CheckOneInstance);
    }

    private void OnDestroy()
    {
        EventsCollection.OnAssetCreate.RemoveListener(CheckOneInstance);
    }
    private void CheckOneInstance(string assetPath)
    {
        var assetsGuides = Resources.LoadAll<SO_ResourceManager>("ScriptableObjects/Managers");

        if (assetsGuides.Length > 1) Debug.LogWarning("Resource Manager is already exists");
    }

    private void FindAllCreatedResources()
    {
        InitalizeCollections();
        List<SO_Resource> findedResources = Resources.LoadAll<SO_Resource>("ScriptableObjects/Resource").ToList<SO_Resource>();

        foreach (var resource in findedResources)
        {
            if(!_resourcesTypes.ContainsKey(resource.ResourceName))
            {
                _resourcesTypes.Add(resource.ResourceName, resource);
            }
        }

        foreach(var resourceKey in _resourcesTypes.Keys)
        {
            if(!_resourcesNames.Contains(resourceKey))
            _resourcesNames.Add(resourceKey);
        }
    }


}


