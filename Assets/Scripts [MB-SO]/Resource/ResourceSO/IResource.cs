
public interface IResource : IItem
{
    public string ResourceName { get; set; }
    public int StartingPrice { get; set; }
    public float Resistance { get; set; }

}
