using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "Resource", menuName = "Inventory/Resource", order = 1)]
[Serializable]
public class SO_Resource : ScriptableObject, IResource
{
    [SerializeField] private string _resourceName;
    [SerializeField] private int _startingPrice;
    [SerializeField] private string _identificator = "Resource";
    [SerializeField] private float _resistance;

    public string Identificator { get => _identificator; set => _identificator = value; }
    public string ResourceName { get => _resourceName; set => _resourceName = value; }
    public int StartingPrice { get => _startingPrice; set => _startingPrice = value; }
    public float Resistance { get => _resistance; set => _resistance = value; }

#if UNITY_EDITOR
    public void RenameScripltableObjectAsset()
    {
        string assetPath = UnityEditor.AssetDatabase.GetAssetPath(this.GetInstanceID());
        var newName = _resourceName + _identificator;
        UnityEditor.AssetDatabase.RenameAsset(assetPath, newName);
    }

#endif
    
}

