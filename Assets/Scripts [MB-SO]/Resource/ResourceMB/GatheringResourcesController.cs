using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;
using System.Linq;
public class GatheringResourcesController : MonoBehaviour
{
    private InputController _input;
    private bool _isGathering;
    private PlayerController _playerController;
    //Gathering 

    //Player Component references
    private MeleeRangeCheck _meleeRangeCheck;
    private PlayerAimController _playerAimController;
    private Animator _playerMeleeAnimator;

    //Serialized properties
    [Tooltip("Property describe how many HP player object taking from resource after one pickaxe hit")]
    [SerializeField] private float Damage = 1.0f;

    [Tooltip("Referance to GAMEOBJECT melee weapon/tool held by player")]
    [SerializeField] private GameObject playerMeleeTool;
    
    //Manage variables
    GameObject _aimPointingObject;
    List<GameObject> _objectsInPlayerMeleeRange;

    public bool GetGatheringState { get => _isGathering; private set => _isGathering = value; }
    public void TakeDamage(GameObject structedResource)
    {
        TakeMeleeRangeAndAimInfo(out _aimPointingObject, out _objectsInPlayerMeleeRange);
        TakeDamageInMeleeRangeAndPontingAt( objectsTags: new string[] {"Resource" });
    }
    private void OnEnable()
    {
        _input.Enable();
        EventsCollection.OnResourceHit.AddListener(TakeDamage);

    }

    private void OnDestroy()
    {
        _input.Disable();
        EventsCollection.OnResourceHit.RemoveListener(TakeDamage);
    }

    #region TakeDamage
    private void TakeDamageInMeleeRangeAndPontingAt(string[] objectsTags)
    {
        if (IsInMeleeRange())
        {
            if (CheckObjectsTagsMatch(objectsTags))
            {
                TakeDamageToObjectsResistance(Damage);

                if (CheckIfRootHasTag("Player"))
                {
                    EventsCollection.DamageTaken?.Invoke(this.gameObject, _aimPointingObject);
                }
            }
        }
    }

    private void Awake()
    {
        HookGlobalInputDelegates();
    }

    private void Update()
    {
        playerInputHook();
    }

    private void HookGlobalInputDelegates()
    {
        InitalizeInputs();


        _input.Player.Gather.performed += (x) => 
        {
            _isGathering = x.ReadValueAsButton();
        };

        _input.Player.Gather.canceled += (x) =>

        {
            _isGathering = x.ReadValueAsButton();
        };
    }

    private void playerInputHook()
    {
        SetAnimatorParameterBool("GatherignResources", _playerMeleeAnimator, _isGathering);

    }

    private void SetAnimatorParameterBool(string propertyName, Animator meleeAnimator, bool value)
    {
        if(meleeAnimator != null)
        {
            _playerMeleeAnimator.SetBool(propertyName, value);
        }
    }

    private bool CheckIfRootHasTag(string tag)
    {
        return GameObject.FindGameObjectsWithTag(tag)
            .Any(x => x.gameObject == this.gameObject);
    }

    private void TakeDamageToObjectsResistance(float resistanceDamage)
    {
        _aimPointingObject.GetComponent<ResourceIdentifier>().Resistance -= resistanceDamage;
        EventsCollection.AfterResourceHit?.Invoke(_aimPointingObject);
    }

    private bool CheckObjectsTagsMatch(string[] objectsTags)
    {
        return objectsTags.Any(x => x == _aimPointingObject.gameObject.tag);
    }

    private bool IsInMeleeRange()
    {
        return _aimPointingObject != null
                            && _objectsInPlayerMeleeRange.Contains(_aimPointingObject);
    }

    #endregion
    public void TakeMeleeRangeAndAimInfo(out GameObject aimPointingObject, out List<GameObject> _objectsInPlayerMeleeRange)
    {
        aimPointingObject = _playerAimController.PointingAtObject;
        _objectsInPlayerMeleeRange = _meleeRangeCheck.meleeRangeInfoBase.GameObjectsInMeleeRangeList;
    }
    private void Start()
    {
        GetPlayerComponets();
    }
    private void InitalizeInputs()
    {
        _input = new InputController();
    }
    private void GetPlayerComponets()
    {
        _meleeRangeCheck = GetComponentInParent<MeleeRangeCheck>();
        _playerAimController = GetComponentInParent<PlayerAimController>();
        _playerController = GetComponentInParent<PlayerController>();

        if(playerMeleeTool.TryGetComponent<Animator>(out var animator))
        {
            _playerMeleeAnimator = animator;
            if (animator.isInitialized) Debug.Log($"Melee Animatior was initalized propertly, with parameters count: {animator.parameterCount}");
        }
    }
}
