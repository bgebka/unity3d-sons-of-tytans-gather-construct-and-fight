using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ResourceIdentifier: MonoBehaviour
{
    [SerializeField] private string _resourceName;
    [SerializeField] private float _health; // resistance * 10;
    [SerializeField] private int _price;

    [Space(50)]
    public SO_ResourceManager resourceManager;
    private Dictionary<string, SO_Resource> _resourceDictionary;

    [HideInInspector]
    public int listID;
    [HideInInspector]
    public List<string> _resourceNamesList;

    public string ResourceName { get => _resourceName; set => _resourceName = value; }
    public int StartingPrice { get => _price; set => _price = value; }
    public float Resistance { get => _health; set => _health =  value; }


    private void OnValidate()
    {
       GetResourceList();
       GetResourceDataFromSO();
    }


    private void Awake()
    {
        InitalizeCollections();
        GetResourceList();
    }

    private void InitalizeCollections()
    {
        _resourceNamesList = new List<string>();
        _resourceDictionary = new Dictionary<string, SO_Resource>();
    }

    private void Reset()
    {
        GetResourceList();
        GetResourceDataFromSO();
    }

    private void GetResourceList()
    {
        _resourceNamesList = resourceManager.ResourcesNames;
    }
    
    public void GetResourceDataFromSO()
    {

        _resourceDictionary = resourceManager.ResourcesTypes;

        if(_resourceDictionary != null)
        {
            var tempSO = _resourceDictionary[_resourceNamesList[listID]] as SO_Resource;

            this._health = tempSO.Resistance;
            this._price = tempSO.StartingPrice;
            this._resourceName = tempSO.ResourceName;
        }
        
    }
}
