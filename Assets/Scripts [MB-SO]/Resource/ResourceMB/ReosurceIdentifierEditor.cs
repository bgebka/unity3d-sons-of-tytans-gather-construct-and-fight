using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
[CustomEditor(typeof(ResourceIdentifier))]
public class ReosurceIdentifierEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        ResourceIdentifier script = (ResourceIdentifier)target;

        GUIContent arrayList = new GUIContent("_resourceList");

        if (arrayList != null)
            script.listID = EditorGUILayout.Popup(arrayList, script.listID, script._resourceNamesList.ToArray());

        if (GUILayout.Button("GetStats"))
        {
            script.GetResourceDataFromSO();
        }
    }
}

#endif