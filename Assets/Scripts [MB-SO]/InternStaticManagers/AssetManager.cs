using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
public static class AssetManager
{
    public static void DeleteAllAssetsTypes<T>(string assetName)
    {
        string[] asset = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));

        foreach (var item in asset)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(item);
            AssetDatabase.DeleteAsset(assetPath);
        }
    }

    public static List<T> FindAllAssetsType<T>() where T : UnityEngine.Object
    {
        List<T> assets = new List<T>();
        string[] guid = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));

        foreach (var assetGUID in guid)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(assetGUID);
            T tempAsset = AssetDatabase.LoadAssetAtPath<T>(assetPath);

            if (tempAsset != null)
            {
                assets.Add(tempAsset);
            }
        }

        return assets;
    }



    public class CustomAssetModificationProcessor : UnityEditor.AssetModificationProcessor
    {
        static void OnWillCreateAsset(string assetName)
        {
            EventsCollection.OnAssetCreate?.Invoke(assetName);
            Debug.Log("OnWillCreateAsset is being called with the following asset: " + assetName + ".");
        }

        static string[] OnWillSaveAssets(string[] paths)
        {
            Debug.Log("OnWillSaveAssets");
            foreach (string path in paths)
                Debug.Log(path);
            return paths;
        }
    }

}

#endif