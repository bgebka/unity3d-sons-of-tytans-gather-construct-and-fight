using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvent: IGameEvent
{
    private event Action _action = delegate { };
    public void AddListener(Action listener)
    {
        _action += listener;
    }

    public void RemoveListener(Action listener)
    {
        _action -= listener;
    }
    public void Invoke()
    {
        _action?.Invoke();
    }

}


#region GenericsVersionOfGameEvent
public class GameEvent<T> : IGameEvent<T>
{
    private event Action<T> _action = delegate { };
    public void AddListener(Action<T> listener)
    {
        _action += listener;
    }

    public void RemoveListener(Action<T> listener)
    {
        _action -= listener;
    }
    public void Invoke(T value)
    {
        _action?.Invoke(value);
    }
    
}

public class GameEvent<T, U> : IGameEvent<T, U>
{
    private event Action<T, U> _action = delegate { };
    public void AddListener(Action<T, U> listener)
    {
        _action += listener;
    }

    public void RemoveListener(Action<T, U> listener)
    {
        _action -= listener;
    }
    public void Invoke(T value1, U value2)
    {
        _action?.Invoke(value1, value2);
    }

}

public class GameEvent<T, U, K> : IGameEvent<T, U, K>
{
    private event Action<T, U, K> _action = delegate { };
    public void AddListener(Action<T, U, K> listener)
    {
        _action += listener;
    }

    public void RemoveListener(Action<T, U, K> listener)
    {
        _action -= listener;
    }
    public void Invoke(T value1, U value2, K value3)
    {
        _action?.Invoke(value1, value2, value3);
    }

#endregion
}