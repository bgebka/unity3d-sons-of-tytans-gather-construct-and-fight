using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class EventsCollection 
{
    public static readonly GameEvent<GameObject> OnResourceDestory = new GameEvent<GameObject>();
    public static readonly GameEvent<GameObject> AfterResourceHit = new GameEvent<GameObject>();

    public static readonly GameEvent<GameObject> OnResourceHit = new GameEvent<GameObject>();
    public static readonly GameEvent<GameObject> OnPointingAt = new GameEvent<GameObject>();

    public static readonly GameEvent<GameObject, GameObject> OnLastHit = new GameEvent<GameObject, GameObject>();
    public static readonly GameEvent<GameObject, GameObject> DamageTaken = new GameEvent<GameObject, GameObject>();

    public static readonly GameEvent<string> OnAssetCreate = new GameEvent<string>();
    public static readonly GameEvent<string> OnAssetDelete = new GameEvent<string>();

    public static readonly GameEvent SO_Update = new GameEvent();

#if UNITY_EDITOR

    #region AssetEventsInvoke
    public class CustomAssetModificationProcessor : UnityEditor.AssetModificationProcessor
    {
        static void OnWillCreateAsset(string assetName)
        {
            OnAssetCreate?.Invoke(assetName);
        }
        
    }

    #endregion

#endif
}
