using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISlot<T> where T : notnull, IItem
{
    public Sprite SlotSprite { get; set; }
    public T SlotItem { get; set; }
}

