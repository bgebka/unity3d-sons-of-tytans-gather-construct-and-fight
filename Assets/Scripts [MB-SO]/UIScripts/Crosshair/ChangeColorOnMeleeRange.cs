using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChangeColorOnMeleeRange : MonoBehaviour
{
    public GameObject Player;

    public Color discable;
    public Color enable;

    private GameObject _aimOnObject;
    private List<GameObject> _objectsInMeleeRange;

    private void Start()
    {
        _aimOnObject = Player.GetComponent<PlayerAimController>().PointingAtObject;
        _objectsInMeleeRange = Player.GetComponent<MeleeRangeCheck>().GetObjectsInMeleeList;
    }

    private void OnEnable()
    {
        EventsCollection.OnPointingAt.AddListener(ChangeCrosshairColor);
    }

    private void Update()
    {
        GetDataAndUpdateAimOnObject();
    }

    private void OnDisable()
    {
        EventsCollection.OnPointingAt.RemoveListener(ChangeCrosshairColor);
    }


    private void GetDataAndUpdateAimOnObject()
    {
        GetComponents();
        ChangeCrosshairColor(_aimOnObject);
    }

    private void GetComponents()
    {
        _aimOnObject = Player.GetComponent<PlayerAimController>().PointingAtObject;
        _objectsInMeleeRange = Player.GetComponent<MeleeRangeCheck>().GetObjectsInMeleeList;
    }

    public void ChangeCrosshairColor(GameObject aimedInObject)
    {
        if(_objectsInMeleeRange.Contains(_aimOnObject))
        {
            GetComponent<Image>().color = enable;
        }
        else
            GetComponent<Image>().color = discable;
    }
}
