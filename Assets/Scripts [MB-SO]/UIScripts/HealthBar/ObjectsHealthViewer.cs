using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsHealthViewer : MonoBehaviour
{
    private const string RemoveSegmentsName = "_RemoveSegments";
    private const string SegmentCountName = "_SegmentCount";

    //GameObjects References
    [SerializeField] private Material _HealthBarMaterial;
    [SerializeField] private GameObject Player;
    [SerializeField] private SO_ResourceManager resourceManager;
    [SerializeField] private SO_MeleeRangeInfoBase meleeRangeInfo;

    //Player Components References
    private GameObject _playerAimOnObject;

    //Working variables
    private List<GameObject> _inMeleeRangeObjectsList;

    void Start()
    {
        GetPlayerInfo();
        HideHealthbar();
    }
    private void AddEventsListeners()
    {
        EventsCollection.OnResourceHit.AddListener(ChangeHealthBarDuringGatheringResource);
        EventsCollection.OnResourceDestory.AddListener((GameObject gameObject) => { HideHealthbar(); });
    }
    private void OnEnable()
    {
        AddEventsListeners();
    }

    private void OnDisable()
    {
        AddEventsListeners();
    }
    private void Update()
    {
        ChangeHealthBarDuringGatheringResource(null);
    }
    private void GetPlayerInfo()
    {
        _playerAimOnObject = Player.GetComponent<PlayerAimController>().PointingAtObject;
        _inMeleeRangeObjectsList = meleeRangeInfo.GameObjectsInMeleeRangeList;
    }
    
    private void ChangeHealthBarDuringGatheringResource(GameObject _struckedResource)
    {
        GetPlayerInfo();

        if (IsInMeleeRange())
        {
            if (CheckIfPointingObjcetHasComponents<ResourceIdentifier>(out var identifier))
            {
                CalculateHealthbarRemoveSegmentsValue(identifier);
            }
            else
            {
                HideHealthbar();
            }
        }
        else HideHealthbar();
    }

    private bool CheckIfPointingObjcetHasComponents<T>(out T identifier) where T : Component
    {
        return _playerAimOnObject.TryGetComponent<T>(out identifier);
    }

    private bool IsInMeleeRange()
    {
        return _playerAimOnObject && _inMeleeRangeObjectsList.Contains(_playerAimOnObject);
    }

    private void CalculateHealthbarRemoveSegmentsValue(ResourceIdentifier identifier)
    {
        float HealthBase = GetPattersAssetResistance(identifier);
        CalculateShaderRemoveSegmentValue(identifier, HealthBase);
    }

    private void CalculateShaderRemoveSegmentValue(ResourceIdentifier identifier, float HealthBase)
    {
        //extract variables
        var objectResistance = identifier.Resistance;
        var segmentCount = _HealthBarMaterial.GetFloat(SegmentCountName);

        //calculate of shadergraph _removeSegments value
        float _healthMaterialValue = (HealthBase - objectResistance) * segmentCount / HealthBase;
        _HealthBarMaterial.SetFloat(RemoveSegmentsName, _healthMaterialValue);
    }

    private float GetPattersAssetResistance(ResourceIdentifier identifier)
    {
        return (resourceManager.ResourcesTypes[identifier.ResourceName] as SO_Resource).Resistance;
    }

    private void HideHealthbar()
    {
        var segmentCount = _HealthBarMaterial.GetFloat(SegmentCountName);
        _HealthBarMaterial.SetFloat(RemoveSegmentsName, segmentCount);
    }
}
