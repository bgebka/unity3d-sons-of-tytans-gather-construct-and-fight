using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class DetectTriggeredObject : MonoBehaviour
{
    [SerializeField] private List<GameObject> _triggeredObejctsList;
    private bool IsInitalized = false;
    public List<GameObject> TriggeredObejctsList { get => _triggeredObejctsList; set => _triggeredObejctsList = value; }

    [SerializeField] public SO_MeleeRangeInfoBase meleeRangeInfoBase;

    private void InitalizeTriggeredObjectList()
    {
        if (!IsInitalized && _triggeredObejctsList != null)
        {
            _triggeredObejctsList = new List<GameObject>();
            IsInitalized = _triggeredObejctsList == null ? false : true;
        }
        else return;
    }

    private void DeleteItemFromList(GameObject gameObject)
    {
        _triggeredObejctsList.Where(x => _triggeredObejctsList.Contains(gameObject) == true)
                                            .ToList()
                                            .ForEach(x => _triggeredObejctsList
                                            .Remove(x));
    }
    private void AddItemToList(GameObject gameObject) 
    {
        if(!meleeRangeInfoBase.ContainsGameObject(gameObject)) _triggeredObejctsList.Add(gameObject);
    }
    private void UpdatePlayerInfoBase()
    {
        meleeRangeInfoBase.GameObjectsInMeleeRangeList =  _triggeredObejctsList
                                                          ?? throw new ArgumentNullException(nameof(_triggeredObejctsList)); 
    }
    private void UpdateAndAddTriggeredData(Collider other)
    {
        InitalizeTriggeredObjectList();
        AddItemToList(other.gameObject);
        UpdatePlayerInfoBase();
    }
    private void UpdateAndDeleteTriggeredData(Collider other)
    {
        InitalizeTriggeredObjectList();
        DeleteItemFromList(other.gameObject);
        UpdatePlayerInfoBase();
    }
    private void Awake()
    {
        InitalizeTriggeredObjectList();
    }
    private void OnDestroy()
    {
        meleeRangeInfoBase.GameObjectsInMeleeRangeList.Clear();
    }

    private void OnTriggerEnter(Collider other)
    {
        UpdateAndAddTriggeredData(other);
    }


    private void OnCollisionEnter(Collision collision)
    {
        UpdateAndAddTriggeredData(collision.collider);
    }   

    private void OnCollisionExit(Collision collision)
    {
        UpdateAndDeleteTriggeredData(collision.collider);
    }

    private void OnCollisionStay(Collision collision)
    {
        if(!_triggeredObejctsList.Contains(collision.gameObject))
        {
            UpdateAndAddTriggeredData(collision.collider);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (!_triggeredObejctsList.Contains(other.gameObject))
        {
            UpdateAndAddTriggeredData(other);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        UpdateAndDeleteTriggeredData(other);
    }
}
