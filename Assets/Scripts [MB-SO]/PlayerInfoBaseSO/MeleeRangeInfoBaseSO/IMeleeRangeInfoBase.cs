using System.Collections.Generic;
using UnityEngine;

public interface IMeleeRangeInfoBase  
{
    public List<GameObject> GetObjectsListAccordingToLayerMask(LayerMask layerMask);
}
