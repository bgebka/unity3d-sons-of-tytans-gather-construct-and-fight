using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

[CreateAssetMenu(fileName = "MeleeRangeInfoBase", menuName = "PlayerInfoBase/MeleeRangeInfoBase", order = 1)]
public class SO_MeleeRangeInfoBase : ScriptableObject
{
    [SerializeField] private List<GameObject> _gameObjectsInMeleeRangeList;
    private bool IsInitialized = false;

    private void OnEnable()
    {
        InitalizeList();
    }

    private void InitalizeList()
    {
        if (!IsInitialized && _gameObjectsInMeleeRangeList == null)
        {
            CreateListInstance();
        }
        else return;
    }

    private void CreateListInstance()
    {
        _gameObjectsInMeleeRangeList = new List<GameObject>();
        IsInitialized = _gameObjectsInMeleeRangeList == null ? false : true;
    }

    public List<GameObject> GameObjectsInMeleeRangeList { get => _gameObjectsInMeleeRangeList; 
                                                          set => _gameObjectsInMeleeRangeList = value; }
   

    public void SetObjectsListAccordingToLayerMask(LayerMask layerMask, List<GameObject> detectedObjects)
    {
        this._gameObjectsInMeleeRangeList = detectedObjects.Where(x => x.gameObject.layer == layerMask).ToList();
    }

    public bool ContainsGameObject(GameObject gameObject) => _gameObjectsInMeleeRangeList.Contains(gameObject);

}
