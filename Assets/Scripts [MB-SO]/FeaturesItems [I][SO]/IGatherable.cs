public interface IGatherable 
{
    public void PlayGatheringAnimation();

    public void PlayCollectionFinishedAnimation();
}
