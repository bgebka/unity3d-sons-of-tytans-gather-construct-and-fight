using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAimController : MonoBehaviour
{

    [SerializeField] private CinemachineVirtualCamera _palyerVirtualCamera;
    [SerializeField] private float _normalSensitivity;
    [SerializeField] private float _aimSensitivity;
    [SerializeField] private float _gizmosRayLength;

    [SerializeField] private Camera _playerCamera;
    [SerializeField] private LayerMask _playerAimMarkingLayer;

    [Tooltip("Its a object is activate in rays colide-position and has propose to trigerring events")]
    [SerializeField] private GameObject _aimPointTrigger;

    [SerializeField] private GameObject _pointingAtObject;
    public GameObject PointingAtObject { get => _pointingAtObject; set => _pointingAtObject = value; }


    private void Update()
    {
        _pointingAtObject =  GetObjectWhichAimingFor();
    }

    public GameObject GetObjectWhichAimingFor()
    {
        Ray ray = ProvideRayFromCenterOfScreen();

        if (Physics.Raycast(ray, out RaycastHit raycastHit, _gizmosRayLength, _playerAimMarkingLayer, QueryTriggerInteraction.Collide))
        {
            _aimPointTrigger.SetActive(true);
            Debug.DrawRay(ray.origin, ray.direction * 20.0f, Color.red);

            _aimPointTrigger.transform.position = raycastHit.point;
            EventsCollection.OnPointingAt?.Invoke(_pointingAtObject);

            return raycastHit.collider.gameObject;
        }
        else
        {
            _aimPointTrigger.SetActive(false);
            return null;
        }
    }

    private Ray ProvideRayFromCenterOfScreen()
    {
        Vector2 screenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
        return _playerCamera.ScreenPointToRay(screenCenterPoint);
    }


}

