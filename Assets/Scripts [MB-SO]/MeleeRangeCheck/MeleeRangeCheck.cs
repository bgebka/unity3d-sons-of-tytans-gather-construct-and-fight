using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

[ExecuteAlways]
public class MeleeRangeCheck : MonoBehaviour
{
    [SerializeField] private List<GameObject> _objectsInMeleeRangeList;
    [SerializeField] public bool isInitalized = false;
    [SerializeField] public LayerMask detectionLayerMask;

    //SphereParameters
    [SerializeField] private float _detectionRange;

    //Gizmos
    [SerializeField] private bool _gizmosActivated;
    [SerializeField] private Color _gizmosColor;

    //Public References
    [SerializeField] public GameObject meleeRangeDetectionSphere;
    [SerializeField] public SO_MeleeRangeInfoBase meleeRangeInfoBase;

    private const float detectionRangeDevider = 2.0f;

    public bool GizmosActivated { get => _gizmosActivated; set => _gizmosActivated = value; }
    public float DetectionRange { get => _detectionRange; set => _detectionRange = value; }
    public List<GameObject> GetObjectsInMeleeList { get => _objectsInMeleeRangeList; set => _objectsInMeleeRangeList = value; }


    private void InitalizeObjectInMeleeRangeList()
    {
        if (!isInitalized && _objectsInMeleeRangeList == null)
        {
            _objectsInMeleeRangeList = new List<GameObject>();
            isInitalized = _objectsInMeleeRangeList != null ? true : false;
        }
        else return;
    }

    private void OnEnable()
    {
        EventsCollection.OnResourceDestory.AddListener(RemoveDestoryedObjectFromList());
    }

    private Action<GameObject> RemoveDestoryedObjectFromList()
    {
        return (GameObject destoryedResource) =>
        {
            if (_objectsInMeleeRangeList.Contains(destoryedResource)) _objectsInMeleeRangeList.Remove(destoryedResource);
        };
    }

    private void OnDisable()
    {
        EventsCollection.OnResourceDestory.RemoveListener(RemoveDestoryedObjectFromList());
    }

    private void Awake()
    {
        InitalizeObjectInMeleeRangeList();
    }
    private void Update()
    {
        CheckHitSphereList();
    }
    private void CheckHitSphereList()
    {
        _objectsInMeleeRangeList = meleeRangeInfoBase.GameObjectsInMeleeRangeList;
    }

    public void ChangeDetectionRangeSphere()
    {
        meleeRangeDetectionSphere.transform.localScale = new Vector3(_detectionRange, _detectionRange, _detectionRange);
    }

    private void OnDrawGizmos()
    {
        if(_gizmosActivated)
        {
            DrawGizmosSphere();
        }
    }

    private void DrawGizmosSphere()
    {
        Gizmos.color = _gizmosColor;
        Gizmos.DrawSphere(meleeRangeDetectionSphere.transform.position, _detectionRange / detectionRangeDevider);
    }
}
