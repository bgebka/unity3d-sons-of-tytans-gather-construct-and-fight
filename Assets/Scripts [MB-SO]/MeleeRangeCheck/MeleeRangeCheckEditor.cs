using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

[CustomEditor(typeof(MeleeRangeCheck))]
public class MeleeRangeCheckEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        MeleeRangeCheck myResource = (MeleeRangeCheck)target;

        if (GUILayout.Button("ChangeDetectionRange"))
        {
            myResource.ChangeDetectionRangeSphere();
        }
    }
}

#endif