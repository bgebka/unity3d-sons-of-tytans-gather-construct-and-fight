using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditionalShaderChannelsDelete : MonoBehaviour
{
    Canvas canvas;

    private void Awake()
    {
        canvas = GetComponent<Canvas>();
    }

    private void Update()
    {
        canvas.additionalShaderChannels = AdditionalCanvasShaderChannels.None;
    }
}
