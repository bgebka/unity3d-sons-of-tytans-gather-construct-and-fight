using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ResourceDestroy : MonoBehaviour
{
    //Working objects
    private ResourceIdentifier _identifier;
    private Hashtable hashtable;
    
    private void Start()
    {
        _identifier = GetComponent<ResourceIdentifier>();
    }

    private void OnEnable()
    {
        EventsCollection.AfterResourceHit.AddListener(ResourceDestoryHandle);
        EventsCollection.DamageTaken.AddListener(LastHitHandling);
        EventsCollection.OnLastHit.AddListener(GatheredAction);
    }

    private void OnDisable()
    {
        EventsCollection.AfterResourceHit.RemoveListener(ResourceDestoryHandle);
        EventsCollection.DamageTaken.RemoveListener(LastHitHandling);
        EventsCollection.OnLastHit.RemoveListener(GatheredAction);
    }

    void ResourceDestoryHandle(GameObject _resource)
    {
       
    }
    private void LastHitHandling(GameObject _PlayerType, GameObject _resourceHitObject)
    {
        if(_identifier.Resistance <= 0.0f && _resourceHitObject == this.gameObject)
        {
            EventsCollection.OnLastHit?.Invoke(_PlayerType, this.gameObject);
        }
    }
    private void GatheredAction(GameObject _PlayerType, GameObject _resourceLastHit)
    {
        if(this.gameObject == _resourceLastHit)
        {
            Transform playerInventory = _PlayerType.transform.GetChild(0);

            if(playerInventory != null)
            {
                this.gameObject.transform.SetParent(playerInventory);
            }
        }
    }
}
