using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SO_UpdateHook : MonoBehaviour
{
    private void Update()
    {
        EventsCollection.SO_Update?.Invoke();
    }
}
